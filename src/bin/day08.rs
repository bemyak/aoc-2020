use aoc_2020::DummyError;
use std::{collections::HashSet, error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input08")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<i32, Box<dyn Error + Send + Sync>> {
    let input: Vec<_> = input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| s.trim())
        .collect();

    let result = execute(&input, HashSet::new(), 0, 0, true).ok_or(DummyError {})?;

    Ok(result)
}

fn execute(
    input: &[&str],
    mut visited: HashSet<usize>,
    mut loc: usize,
    mut acc: i32,
    allow_fork: bool,
) -> Option<i32> {
    loop {
        if loc >= input.len() {
            break;
        }
        if visited.contains(&loc) {
            break;
        }
        visited.insert(loc);
        let line = input[loc].chars().collect::<Vec<_>>();
        let instruction = &line[..3].iter().collect::<String>();
        let shift = &line[4..].iter().collect::<String>().parse::<i32>().unwrap();

        match instruction.as_ref() {
            "acc" => {
                acc += shift;
                loc += 1;
            }
            "jmp" => {
                if allow_fork {
                    if let Some(result) = execute(input, visited.clone(), loc + 1, acc, false) {
                        return Some(result);
                    }
                }
                loc = (loc as i32 + shift) as usize;
            }
            "nop" => {
                if allow_fork {
                    if let Some(result) = execute(
                        input,
                        visited.clone(),
                        (loc as i32 + shift) as usize,
                        acc,
                        false,
                    ) {
                        return Some(result);
                    }
                }
                loc += 1;
            }
            _ => panic!("Bad instruction"),
        }
    }

    if loc == input.len() {
        Some(acc)
    } else {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"nop +0
        acc +1
        jmp +4
        acc +3
        jmp -3
        acc -99
        acc +1
        jmp -4
        acc +6";
        let result = solve(input);
        assert!(result.is_ok());
        let result = result.unwrap();
        assert_eq!(result, 8);
    }
}
