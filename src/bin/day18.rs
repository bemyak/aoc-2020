#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input18")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<u64, Box<dyn Error + Send + Sync>> {
    let result = input
        .split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .map(|s| calc(s))
        .sum();
    Ok(result)
}

fn calc_inner(s: &str) -> (u64, usize) {
    let mut result = 0;
    let mut bracket_stack: Vec<usize> = Vec::new();
    let mut last_i = usize::MAX;
    for (i, c) in s.chars().enumerate() {
        if last_i != usize::MAX && i <= last_i {
            continue;
        }
        last_i = i;
        match (bracket_stack.len(), c) {
            (_, ' ') => {}
            (0, '0'..='9') => {
                let d = c.to_digit(10).unwrap() as u64;
                result += d;
            }
            (0, '+') => {}
            (0, '*') => {
                let (res, last_j) = calc_inner(&s[i + 1..]);
                result *= res;
                last_i = i + last_j + 1;
            }
            (_, '(') => bracket_stack.push(i),
            (_, ')') => {
                if let Some(j) = bracket_stack.pop() {
                    if bracket_stack.is_empty() {
                        let (bracket_result, _) = calc_inner(&s[j + 1..i]);
                        result += bracket_result;
                    }
                }
            }
            _ => {}
        }
    }

    (result, last_i)
}

fn calc(s: &str) -> u64 {
    let (r, _) = calc_inner(s);
    r
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        assert_eq!(calc("2 + 2"), 4);
        assert_eq!(calc("2 * 2"), 4);
        assert_eq!(calc("1 + (2 * 3) + (4 * (5 + 6))"), 51);
        assert_eq!(calc("2 * 3 + (4 * 5)"), 46);
        assert_eq!(calc("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 1445);
        assert_eq!(calc("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"), 669060);
        assert_eq!(
            calc("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"),
            23340
        );
    }
}
