#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{error::Error, fs, ops::Range};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input16")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let input: Vec<_> = input
        .split("\n\n")
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();

    let rules = parse_field_rules(input[0]);
    println!("{:?}", rules);
    let your = parse_your_ticket(input[1]);
    let others = parse_nearby_ticket(input[2]);

    let fields = resolve_fields(&others, &rules);
    let result = your
        .iter()
        .enumerate()
        .filter_map(|(i, value)| if fields[i] <= 5 { Some(value) } else { None })
        .product();

    Ok(result)
}

fn resolve_fields(others: &[Vec<usize>], rules: &[(Range<usize>, Range<usize>)]) -> Vec<usize> {
    let field_analysis = others
        .iter()
        .map(|ticket| {
            ticket
                .iter()
                .map(|field| find_matching_rules(field, &rules))
                .collect::<Vec<_>>()
        })
        .filter(|ticket| !ticket.iter().any(|rules| rules.is_empty()))
        .collect::<Vec<_>>();

    let all_rules: Vec<usize> = (0..rules.len()).collect();
    let mut cache: Vec<Vec<usize>> = vec![all_rules; rules.len()];

    loop {
        if cache.iter().any(|rules| rules.is_empty()) {
            panic!("BAD");
        }
        if !cache.iter().any(|rules| rules.len() != 1) {
            break;
        }

        for ticket in &field_analysis {
            for (i, field) in ticket.iter().enumerate() {
                let new_c = cache[i]
                    .iter()
                    .filter(|&r| field.contains(r))
                    .copied()
                    .collect::<Vec<_>>();
                let _ = std::mem::replace(&mut cache[i], new_c);
            }
        }

        let unique_indexes: Vec<_> = cache
            .iter()
            .enumerate()
            .filter(|(_, rules)| rules.len() == 1)
            .map(|(i, rules)| (i, rules[0]))
            .collect();
        for (unique_index, unique_rule) in unique_indexes {
            for (i, c) in cache.iter_mut().enumerate() {
                if i == unique_index {
                    continue;
                }
                let new_c = c
                    .iter()
                    .filter(|&&r| r != unique_rule)
                    .copied()
                    .collect::<Vec<_>>();
                let _ = std::mem::replace(c, new_c);
            }
        }

        println!("{:?}", cache);
    }

    let result: Vec<usize> = cache.into_iter().flatten().collect();

    println!("{:?}", result);

    result
}

fn parse_field_rules(s: &str) -> Vec<(Range<usize>, Range<usize>)> {
    s.split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .map(|s| {
            let (_, ranges) = s.split_once_s(": ").unwrap();
            let (r1, r2) = ranges.split_once_s(" or ").unwrap();

            let (r1l, r1h) = r1.split_once_('-').unwrap();
            let r1l = r1l.parse::<usize>().unwrap();
            let r1h = r1h.parse::<usize>().unwrap();

            let (r2l, r2h) = r2.split_once_('-').unwrap();
            let r2l = r2l.parse::<usize>().unwrap();
            let r2h = r2h.parse::<usize>().unwrap();

            ((r1l..r1h + 1), (r2l..r2h + 1))
        })
        .collect()
}

fn parse_your_ticket(s: &str) -> Vec<usize> {
    s.split('\n')
        .nth(1)
        .unwrap()
        .split(',')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .filter_map(|s| s.parse::<usize>().ok())
        .collect()
}

fn parse_nearby_ticket(s: &str) -> Vec<Vec<usize>> {
    s.split('\n')
        .skip(1)
        .map(|s| {
            s.split(',')
                .map(|s| s.trim())
                .filter(|s| !s.is_empty())
                .filter_map(|s| s.parse::<usize>().ok())
                .collect()
        })
        .collect()
}

fn find_matching_rules(field: &usize, rules: &[(Range<usize>, Range<usize>)]) -> Vec<usize> {
    let mut result = Vec::new();
    for (i, (r1, r2)) in rules.iter().enumerate() {
        if r1.contains(field) || r2.contains(field) {
            result.push(i);
        }
    }
    result
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets
3,9,18
15,1,5
5,14,9";
        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 11 * 12 * 13);

        let input: Vec<_> = input
            .split("\n\n")
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .collect();

        let rules = parse_field_rules(input[0]);
        println!("{:?}", rules);
        let others = parse_nearby_ticket(input[2]);

        let fields = resolve_fields(&others, &rules);
        assert_eq!(fields, [1, 0, 2]);
    }
}
