use std::{
    cmp::{max, min},
    collections::{HashMap, HashSet},
    error::Error,
    fs, print,
};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct D3 {
    x: i32,
    y: i32,
    z: i32,
    w: i32,
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input17")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let mut active_cubes = cubes_from_input(input);
    for _ in 0..6 {
        round(&mut active_cubes);
    }
    Ok(active_cubes.len())
}

fn round(active_cubes: &mut HashSet<D3>) {
    let mut neighbors: HashMap<D3, usize> = HashMap::new();
    for cube in active_cubes.iter() {
        for xi in -1..=1 {
            for yi in -1..=1 {
                for zi in -1..=1 {
                    for wi in -1..=1 {
                        if xi == 0 && yi == 0 && zi == 0 && wi == 0 {
                            continue;
                        }
                        let neighbor_d3 = D3 {
                            x: cube.x + xi,
                            y: cube.y + yi,
                            z: cube.z + zi,
                            w: cube.w + wi,
                        };
                        let n = neighbors.entry(neighbor_d3).or_insert(0);
                        *n += 1;
                    }
                }
            }
        }
    }

    let mut new_set = HashSet::new();

    for (cube, n) in neighbors {
        if active_cubes.contains(&cube) {
            if n == 2 || n == 3 {
                new_set.insert(cube);
            }
        } else if n == 3 {
            new_set.insert(cube);
        }
    }

    let _ = std::mem::replace(active_cubes, new_set);
}

fn cubes_from_input(input: &str) -> HashSet<D3> {
    let mut result = HashSet::new();

    let input: Vec<_> = input
        .split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .map(|s| s.chars().collect::<Vec<_>>())
        .collect();

    for (y, row) in input.iter().enumerate() {
        for (x, &c) in row.iter().enumerate() {
            if c == '#' {
                result.insert(D3 {
                    x: x as i32,
                    y: y as i32,
                    z: 0,
                    w: 0,
                });
            }
        }
    }

    result
}

#[allow(dead_code)]
fn print_grid(active_cubes: &HashSet<D3>) {
    let (mut min_x, mut max_x) = (i32::MAX, i32::MIN);
    let (mut min_y, mut max_y) = (i32::MAX, i32::MIN);
    let (mut min_z, mut max_z) = (i32::MAX, i32::MIN);
    let (mut min_w, mut max_w) = (i32::MAX, i32::MIN);

    for cube in active_cubes {
        min_x = min(min_x, cube.x);
        max_x = max(max_x, cube.x);
        min_y = min(min_y, cube.y);
        max_y = max(max_y, cube.y);
        min_z = min(min_z, cube.z);
        max_z = max(max_z, cube.z);
        min_w = min(min_w, cube.w);
        max_w = max(max_w, cube.w);
    }

    for w in min_w..=max_w {
        for z in min_z..=max_z {
            println!("\nz={}, w={}", z, w);
            for y in min_y..=max_y {
                for x in min_x..=max_x {
                    if active_cubes.contains(&D3 { x, y, z, w }) {
                        print!("#");
                    } else {
                        print!(".");
                    }
                }
                println!();
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_round() {
        let input = r".#.
        ..#
        ###";

        println!("Before any cycles:");
        let mut active_cubes = cubes_from_input(input);
        assert_eq!(active_cubes.len(), 5);
        print_grid(&active_cubes);

        println!("\n\nAfter 1 cycle:");
        round(&mut active_cubes);
        assert_eq!(active_cubes.len(), 29);
        print_grid(&active_cubes);

        println!("\n\nAfter 2 cycles:");
        round(&mut active_cubes);
        assert_eq!(active_cubes.len(), 60);
        print_grid(&active_cubes);

        round(&mut active_cubes);
        assert_eq!(active_cubes.len(), 320);

        round(&mut active_cubes);
        assert_eq!(active_cubes.len(), 188);

        round(&mut active_cubes);
        assert_eq!(active_cubes.len(), 1056);

        round(&mut active_cubes);
        assert_eq!(active_cubes.len(), 848);
    }

    #[test]
    fn test() {
        let input = r".#.
        ..#
        ###";
        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 848);
    }
}
