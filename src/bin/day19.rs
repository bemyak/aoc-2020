use aoc_2020::*;
use bitvec::prelude::*;
use std::{collections::HashMap, error::Error, fs};

enum Rule {
    Seq(Vec<usize>),
    Or(Vec<usize>, Vec<usize>),
    End(bool),
}

struct Rules(HashMap<usize, Rule>);

impl Rules {
    fn match_char(&self, ch: bool, s: &BitSlice, q: &mut Vec<usize>) -> bool {
        match s.iter().next() {
            Some(ch1) if ch == *ch1 => self.matches(&s[1..], q),
            _ => false,
        }
    }

    fn match_seq(&self, seq: &[usize], s: &BitSlice, q: &mut Vec<usize>) -> bool {
        seq.iter().rev().for_each(|r| q.push(*r));
        self.matches(s, q)
    }

    fn matches(&self, msg: &BitSlice, mut q: &mut Vec<usize>) -> bool {
        if q.is_empty() && msg.is_empty() {
            return true;
        }
        if q.is_empty() || msg.is_empty() {
            return false;
        }
        let first = &self.0[&q.pop().unwrap()];
        match first {
            Rule::End(ch) => self.match_char(*ch, msg, &mut q),
            Rule::Seq(seq) => self.match_seq(&seq, msg, &mut q),
            Rule::Or(seq1, seq2) => {
                self.match_seq(&seq1, msg, &mut q.clone())
                    || self.match_seq(&seq2, msg, &mut q.clone())
            }
        }
    }
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input19")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let (rules, msgs) = input.split_once_s("\n\n").unwrap();
    let mut rules = parse_rules(rules);
    rules.insert(8, Rule::Or(vec![42], vec![42, 8]));
    rules.insert(11, Rule::Or(vec![42, 31], vec![42, 11, 31]));
    let rules = Rules(rules);

    let result = msgs
        .split('\n')
        .map(str_to_bitvec)
        .filter(|s| rules.matches(s, &mut vec![0]))
        .count();

    Ok(result)
}

fn parse_rules(s: &str) -> HashMap<usize, Rule> {
    s.split('\n')
        .map(|s| s.trim())
        .map(|line| {
            let (n, rule) = line.split_once_s(": ").unwrap();
            let n = n.parse().unwrap();
            let rule: Rule = if rule.contains('"') {
                let c = rule.chars().collect::<Vec<_>>()[1];
                Rule::End(char_to_bool(&c))
            } else if rule.contains('|') {
                let rules: Vec<Vec<usize>> = rule
                    .split('|')
                    .map(|s| s.trim())
                    .map(|list_of_rules| {
                        list_of_rules
                            .split(' ')
                            .map(|s| s.trim())
                            .filter_map(|rule| rule.parse::<usize>().ok())
                            .collect()
                    })
                    .collect();
                Rule::Or(rules[0].clone(), rules[1].clone())
            } else {
                let rules: Vec<usize> = rule
                    .split(' ')
                    .map(|s| s.trim())
                    .filter_map(|rule| rule.parse::<usize>().ok())
                    .collect();
                Rule::Seq(rules)
            };

            (n, rule)
        })
        .collect()
}

fn str_to_bitvec(s: &str) -> BitVec {
    let mut bv = BitVec::<LocalBits, usize>::new();
    for c in s.trim().chars() {
        bv.push(char_to_bool(&c))
    }
    bv
}

fn char_to_bool(c: &char) -> bool {
    match c {
        'a' => false,
        'b' => true,
        _ => panic!("WTF?"),
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_all_possible_recursive() {
        let input = r#"0: 8 11
8: 42 | 42 8
11: 42 31 | 42 11 31
31: 104
42: 104
95: "b"
104: "a"

aaa"#;

        let r1 = solve(input).unwrap();
        assert_eq!(r1, 1);
    }

    #[test]
    fn test() {
        let input = r#"0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb"#;
        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 2);
    }
}
