#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input")?;
    let result = solve(&input);
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> usize {
    let input: Vec<_> = input
        .split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .filter_map(|s| s.parse::<i32>().ok())
        .collect();
    println!("{:?}", input);
    input.len()
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"";
        let result = solve(input);
        assert_eq!(result, 0);
    }
}
