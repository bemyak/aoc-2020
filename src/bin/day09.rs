#[allow(unused_imports)]
use aoc_2020::*;
use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input09")?;
    let result = solve(&input, 25)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str, preamble_len: usize) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let input: Vec<_> = input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| s.trim())
        .filter_map(|s| s.parse::<usize>().ok())
        .collect();
    for i in preamble_len..input.len() {
        if !is_sum_of_two(input[i], &input[i - preamble_len..i]) {
            let x = input[i];
            for j in 0..i {
                if let Some((start, end)) = find(x, &input, j, i) {
                    let r = &input[start..end];
                    return Ok(r.iter().max().unwrap() + r.iter().min().unwrap());
                }
            }
        }
    }
    Err(Box::new(DummyError))
}
fn is_sum_of_two(x: usize, input: &[usize]) -> bool {
    for i in 0..input.len() - 1 {
        for j in i + 1..input.len() {
            if input[i] + input[j] == x {
                return true;
            }
        }
    }
    false
}

fn find(x: usize, input: &[usize], start: usize, end: usize) -> Option<(usize, usize)> {
    let mut sum = 0;
    #[allow(clippy::clippy::needless_range_loop)]
    for i in start..end {
        sum += input[i];
        if sum == x {
            return Some((start, i));
        }
        if sum > x {
            return None;
        }
    }
    None
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"35
        20
        15
        25
        47
        40
        62
        55
        65
        95
        102
        117
        150
        182
        127
        219
        299
        277
        309
        576";

        let result = solve(input, 5);
        assert!(result.is_ok());
        let result = result.unwrap();
        assert_eq!(result, 62);
    }
}
