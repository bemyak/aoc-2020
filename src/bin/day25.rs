#[allow(unused_imports)]
use aoc_2020::EasilyParsable;

const CARD_PUB_KEY: usize = 18499292;
const DOOR_PUB_KEY: usize = 8790390;
const SUBJECT_NUMBER: usize = 7;

fn main() {
    let result = solve(CARD_PUB_KEY, DOOR_PUB_KEY);
    println!("{}", result);
}

fn solve(pk1: usize, pk2: usize) -> usize {
    let ls1 = get_loop_size(pk1, SUBJECT_NUMBER);
    let ls2 = get_loop_size(pk2, SUBJECT_NUMBER);

    let r1 = transform(ls1, pk2);
    let r2 = transform(ls2, pk1);
    assert_eq!(r1, r2);
    r1
}

fn transform(loop_size: usize, subject: usize) -> usize {
    let mut value = 1;
    for _ in 0..loop_size {
        value *= subject;
        value %= 20201227;
    }
    value
}

fn get_loop_size(pk: usize, subject: usize) -> usize {
    let mut loop_size = 0;
    let mut value = 1;
    while value != pk {
        value *= subject;
        value %= 20201227;
        loop_size += 1;
    }
    loop_size
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_get_loop_size() {
        assert_eq!(get_loop_size(5764801, SUBJECT_NUMBER), 8);
        assert_eq!(get_loop_size(17807724, SUBJECT_NUMBER), 11);
    }

    #[test]
    fn test() {
        let result = solve(5764801, 17807724);
        assert_eq!(result, 14897079);
    }
}
