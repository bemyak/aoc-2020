#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{collections::HashMap, error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input14")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let input: Vec<_> = input
        .split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .filter_map(|s| s.split_once_s(" = "))
        .collect();

    let mut mask: &[u8] = &[0];
    let mut mem = HashMap::new();

    for (operation, value) in input {
        match &operation[0..4] {
            "mask" => {
                mask = value.as_bytes();
            }
            "mem[" => {
                let value = value.parse::<usize>().unwrap();
                let addr = &operation[4..operation.len() - 1];
                let addr = addr.parse::<usize>().unwrap();
                store(value, addr, mask, &mut mem, 0);
            }
            _ => {}
        }
    }

    Ok(mem.values().sum())
}

fn store(value: usize, addr: usize, mask: &[u8], mem: &mut HashMap<usize, usize>, i: usize) {
    if i == 36 {
        mem.insert(addr, value);
        return;
    }
    match mask[i] {
        b'0' => store(value, addr, mask, mem, i + 1),
        b'1' => {
            let a = addr | 1 << (35 - i);
            store(value, a, mask, mem, i + 1)
        }
        b'X' => {
            let a1 = addr & !(1 << (35 - i));
            let a2 = addr | 1 << (35 - i);
            store(value, a1, mask, mem, i + 1);
            store(value, a2, mask, mem, i + 1);
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"mask = 000000000000000000000000000000X1001X
        mem[42] = 100
        mask = 00000000000000000000000000000000X0XX
        mem[26] = 1";
        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 208);
    }
}
