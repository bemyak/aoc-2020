#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{cmp::max, error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input10")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let mut input: Vec<_> = input
        .split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .filter_map(|s| s.parse::<usize>().ok())
        .collect();
    input.sort_unstable();

    let mut cache = vec![0; *input.last().unwrap() + 1];
    for &jolt in input.iter() {
        if jolt <= 3 {
            cache[jolt] += 1;
        }

        let start = max(1, jolt as i64 - 3) as usize;
        for j in (start..jolt).rev() {
            cache[jolt] += cache[j];
        }
    }

    println!("{:?}", cache);

    Ok(*cache.last().unwrap())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"16
        10
        15
        5
        1
        11
        7
        19
        6
        12
        4";

        let result = solve(input).unwrap();
        assert_eq!(result, 8);

        let input = r"28
        33
        18
        42
        31
        14
        46
        20
        48
        47
        24
        23
        49
        45
        19
        38
        39
        11
        1
        32
        25
        35
        8
        17
        7
        9
        4
        2
        34
        10
        3";

        let result = solve(input).unwrap();
        assert_eq!(result, 19208);
    }
}
