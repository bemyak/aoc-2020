use std::str::FromStr;
use std::{error::Error, fs};

struct Err;

#[derive(Debug)]
struct Entry {
    min: usize,
    max: usize,
    c: char,
    pass: String,
}

impl FromStr for Entry {
    type Err = Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let p1: Vec<_> = s.split(':').map(|s| s.trim()).collect();
        let pass = p1.get(1).ok_or(Err {})?.to_string();

        let p2: Vec<_> = p1[0].split(' ').collect();
        let c = p2[1].chars().next().unwrap();

        let p3: Vec<_> = p2[0]
            .split('-')
            .filter_map(|s| s.parse::<usize>().ok())
            .collect();
        let min = p3[0];
        let max = p3[1];

        Ok(Self { min, max, c, pass })
    }
}

impl Entry {
    fn is_comply(&self) -> bool {
        let mut result = 0;
        for (i, c) in self.pass.chars().enumerate() {
            if c == self.c && (i + 1 == self.min || i + 1 == self.max) {
                result += 1;
            }
        }
        result == 1
    }
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input02")?;
    let result: Vec<_> = input
        .split('\n')
        .filter_map(|s| Entry::from_str(s).ok())
        .filter(|p| p.is_comply())
        .collect();
    println!("{:?}", result.len());
    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let data: Vec<Entry> = vec!["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"]
            .into_iter()
            .filter_map(|s| Entry::from_str(s).ok())
            .collect();
        assert!(data.len() == 3);
        assert!(data[0].is_comply());
        assert!(!data[1].is_comply());
        assert!(!data[2].is_comply());
    }
}
