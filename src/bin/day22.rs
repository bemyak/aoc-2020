#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{
    collections::{HashSet, VecDeque},
    error::Error,
    fs,
};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input22")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let input: Vec<_> = input
        .split("\n\n")
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .collect();

    assert_eq!(input.len(), 2);
    let d1 = parse_dec(input[0]);
    let d2 = parse_dec(input[1]);
    println!("d1 size = {}, d2 size = {}", d1.len(), d2.len());
    let (p1_won, winner) = play_part2(d1, d2);
    println!("Is p1? {}, {:?}", p1_won, winner);
    let result: usize = winner
        .iter()
        .rev()
        .enumerate()
        .map(|(i, card)| (i + 1) * card)
        .sum();
    Ok(result)
}

#[allow(dead_code)]
fn play_part2(mut d1: VecDeque<usize>, mut d2: VecDeque<usize>) -> (bool, VecDeque<usize>) {
    let mut p1_configurations = HashSet::new();
    let mut p2_configurations = HashSet::new();
    while !d1.is_empty() && !d2.is_empty() {
        if p1_configurations.contains(&d1) && p2_configurations.contains(&d2) {
            return (true, d1);
        } else {
            p1_configurations.insert(d1.clone());
            p2_configurations.insert(d2.clone());
        }
        let c1 = d1.pop_front().unwrap();
        let c2 = d2.pop_front().unwrap();

        let p1_won = if c1 <= d1.len() && c2 <= d2.len() {
            let sub_deck1: VecDeque<usize> = d1.iter().take(c1).cloned().collect();
            let sub_deck2: VecDeque<usize> = d2.iter().take(c2).cloned().collect();
            assert_eq!(c1, sub_deck1.len());
            assert_eq!(c2, sub_deck2.len());
            let (p1_won, _winner_deck) = play_part2(sub_deck1, sub_deck2);
            p1_won
        } else {
            c1 > c2
        };

        let (winner, c1, c2) = if p1_won {
            (&mut d1, c1, c2)
        } else {
            (&mut d2, c2, c1)
        };

        winner.push_back(c1);
        winner.push_back(c2);
    }

    if d1.is_empty() {
        (false, d2)
    } else {
        (true, d1)
    }
}

#[allow(dead_code)]
fn play_part1(mut d1: VecDeque<usize>, mut d2: VecDeque<usize>) -> VecDeque<usize> {
    while !d1.is_empty() && !d2.is_empty() {
        let c1 = d1.pop_front().unwrap();
        let c2 = d2.pop_front().unwrap();
        let (winner, c1, c2) = if c1 > c2 {
            (&mut d1, c1, c2)
        } else {
            (&mut d2, c2, c1)
        };

        winner.push_back(c1);
        winner.push_back(c2);
    }

    if d1.is_empty() {
        d2
    } else {
        d1
    }
}

fn parse_dec(s: &str) -> VecDeque<usize> {
    s.split('\n')
        .map(str::trim)
        .skip(1)
        .filter_map(|card| card.parse::<usize>().ok())
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"Player 1:
        9
        2
        6
        3
        1

        Player 2:
        5
        8
        4
        7
        10";
        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 291);
    }
    #[test]
    fn test_infinite() {
        let input = r"Player 1:
        43
        19

        Player 2:
        2
        29
        14";
        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 105);
    }
}
