#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use regex::Regex;
use std::{
    collections::{HashMap, HashSet},
    error::Error,
    fs,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Direction {
    NE,
    NW,
    SE,
    SW,
    E,
    W,
}

// Thanks to this article: https://www.redblobgames.com/grids/hexagons/
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Coord {
    x: i32,
    y: i32,
    z: i32,
}

impl Coord {
    fn new() -> Self {
        Coord { x: 0, y: 0, z: 0 }
    }

    fn shift(&mut self, d: Direction) {
        match d {
            Direction::NE => {
                self.x += 1;
                self.z -= 1;
            }
            Direction::NW => {
                self.y += 1;
                self.z -= 1;
            }
            Direction::SE => {
                self.y -= 1;
                self.z += 1;
            }
            Direction::SW => {
                self.x -= 1;
                self.z += 1;
            }
            Direction::E => {
                self.x += 1;
                self.y -= 1;
            }
            Direction::W => {
                self.x -= 1;
                self.y += 1;
            }
        }
        assert_eq!(self.x + self.y + self.z, 0);
    }

    fn get_neighbor(&self, d: Direction) -> Self {
        let mut n = *self;
        n.shift(d);
        n
    }

    fn get_neighbors(&self) -> [Self; 6] {
        [
            self.get_neighbor(Direction::NE),
            self.get_neighbor(Direction::NW),
            self.get_neighbor(Direction::SE),
            self.get_neighbor(Direction::SW),
            self.get_neighbor(Direction::E),
            self.get_neighbor(Direction::W),
        ]
    }
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input24")?;
    let result = solve(&input, 100);
    println!("{}", result.len());
    Ok(())
}

fn init(directions: &[Vec<Direction>]) -> HashSet<Coord> {
    let mut tiles = HashSet::new();

    for tile in directions {
        let mut coord = Coord::new();
        for &d in tile {
            coord.shift(d);
        }
        if tiles.contains(&coord) {
            tiles.remove(&coord);
        } else {
            tiles.insert(coord);
        }
    }
    tiles
}

fn day(tiles: &mut HashSet<Coord>) {
    let mut neighbors = HashMap::new();
    for tile in tiles.iter() {
        let tile_neighbors = tile.get_neighbors();
        for neighbor in tile_neighbors.iter() {
            let my_counter = neighbors.entry(*tile).or_insert(0);
            if tiles.contains(neighbor) {
                *my_counter += 1;
            } else if tiles.contains(tile) {
                let n = neighbors.entry(*neighbor).or_insert(0);
                *n += 1;
            }
        }
    }
    for (coord, n) in neighbors {
        let is_black = tiles.contains(&coord);
        if is_black {
            if n == 0 || n > 2 {
                tiles.remove(&coord);
            }
        } else if n == 2 {
            tiles.insert(coord);
        }
    }
}

fn parse_input(input: &str) -> Vec<Vec<Direction>> {
    let direction_regex: Regex = Regex::new(r"((?:s|n)?(?:e|w))").unwrap();
    input
        .split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .map(|s| {
            direction_regex
                .find_iter(s)
                .map(|match_| {
                    let s = match_.as_str();
                    match s {
                        "ne" => Direction::NE,
                        "nw" => Direction::NW,
                        "se" => Direction::SE,
                        "sw" => Direction::SW,
                        "e" => Direction::E,
                        "w" => Direction::W,
                        _ => unreachable!(),
                    }
                })
                .collect::<Vec<_>>()
        })
        .collect()
}

fn solve(input: &str, days: usize) -> HashSet<Coord> {
    let input: Vec<_> = parse_input(input);

    let mut tiles = init(&input);

    for _ in 0..days {
        day(&mut tiles);
    }
    tiles
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"sesenwnenenewseeswwswswwnenewsewsw
        neeenesenwnwwswnenewnwwsewnenwseswesw
        seswneswswsenwwnwse
        nwnwneseeswswnenewneswwnewseswneseene
        swweswneswnenwsewnwneneseenw
        eesenwseswswnenwswnwnwsewwnwsene
        sewnenenenesenwsewnenwwwse
        wenwwweseeeweswwwnwwe
        wsweesenenewnwwnwsenewsenwwsesesenwne
        neeswseenwwswnwswswnw
        nenwswwsewswnenenewsenwsenwnesesenew
        enewnwewneswsewnwswenweswnenwsenwsw
        sweneswneswneneenwnewenewwneswswnese
        swwesenesewenwneswnwwneseswwne
        enesenwswwswneneswsenwnewswseenwsese
        wnwnesenesenenwwnenwsewesewsesesew
        nenewswnwewswnenesenwnesewesw
        eneswnwswnwsenenwnwnwwseeswneewsenese
        neswnwewnwnwseenwseesewsenwsweewe
        wseweeenwnesenwwwswnew";

        let directions = parse_input(input);
        let mut tiles = init(&directions);
        day(&mut tiles);
        assert_eq!(tiles.len(), 15);
        day(&mut tiles);
        assert_eq!(tiles.len(), 12);
        day(&mut tiles);
        assert_eq!(tiles.len(), 25);
        day(&mut tiles);
        assert_eq!(tiles.len(), 14);
        day(&mut tiles);
        assert_eq!(tiles.len(), 23);
        day(&mut tiles);
        assert_eq!(tiles.len(), 28);
        day(&mut tiles);
        assert_eq!(tiles.len(), 41);
        day(&mut tiles);
        assert_eq!(tiles.len(), 37);
        day(&mut tiles);
        assert_eq!(tiles.len(), 49);
        day(&mut tiles);
        assert_eq!(tiles.len(), 37);
        for _ in 0..10 {
            day(&mut tiles);
        }
        assert_eq!(tiles.len(), 132);
        for _ in 0..10 {
            day(&mut tiles);
        }
        assert_eq!(tiles.len(), 259);
        for _ in 0..10 {
            day(&mut tiles);
        }
        assert_eq!(tiles.len(), 406);
        for _ in 0..10 {
            day(&mut tiles);
        }
        assert_eq!(tiles.len(), 566);
        for _ in 0..10 {
            day(&mut tiles);
        }
        assert_eq!(tiles.len(), 788);
        for _ in 0..10 {
            day(&mut tiles);
        }
        assert_eq!(tiles.len(), 1106);
        for _ in 0..10 {
            day(&mut tiles);
        }
        assert_eq!(tiles.len(), 1373);
        for _ in 0..10 {
            day(&mut tiles);
        }
        assert_eq!(tiles.len(), 1844);
        for _ in 0..10 {
            day(&mut tiles);
        }
        assert_eq!(tiles.len(), 2208);
    }
}
