#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input11")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let mut input: Vec<_> = input
        .split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .map(|s| s.chars().collect::<Vec<_>>())
        .collect();

    let mut n_round = 0;
    loop {
        let (new_input, changes) = round(&input);

        // println!("======= ROUND {} ========", n_round);
        // for r in &new_input {
        //     for seat in r {
        //         print!("{}", seat);
        //     }
        //     println!();
        // }

        if changes == 0 {
            break;
        }
        n_round += 1;
        input = new_input;
    }

    let mut count_occupied = 0;
    for r in input {
        for seat in r {
            if seat == '#' {
                count_occupied += 1;
            }
        }
    }

    println!("number of iterations: {:?}", n_round);
    Ok(count_occupied)
}

fn round(input: &[Vec<char>]) -> (Vec<Vec<char>>, usize) {
    let mut result = input.to_vec();
    let mut changes = 0;

    for r in 0..input.len() {
        for c in 0..input[0].len() {
            if input[r][c] == '.' {
                continue;
            }
            let occupied = count_occupied(input, r, c);
            if occupied == 0 && result[r][c] != '#' {
                result[r][c] = '#';
                changes += 1;
            } else if occupied >= 5 && result[r][c] != 'L' {
                result[r][c] = 'L';
                changes += 1;
            }
        }
    }

    (result, changes)
}

fn count_occupied(input: &[Vec<char>], y: usize, x: usize) -> usize {
    // println!("{}", input[y][x]);

    let mut result = 0;
    for p in -1..=1 {
        for q in -1..=1 {
            if p == 0 && q == 0 {
                continue;
            }
            let mut k = 1;
            loop {
                let (x1, y1) = (x as i32 + (p * k), y as i32 + (q * k));
                // println!("{}, {}", x1, y1);
                if x1 >= input[0].len() as i32 || y1 >= input.len() as i32 || x1 < 0 || y1 < 0 {
                    break;
                }

                let c = input[y1 as usize][x1 as usize];

                if c == '#' {
                    result += 1;
                    break;
                }
                if c == 'L' {
                    break;
                }
                k += 1;
            }
        }
    }

    result
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_occupied() {
        let input = r".......#.
        ...#.....
        .#.......
        .........
        ..#L....#
        ....#....
        .........
        #........
        ...#.....";

        let input: Vec<_> = input
            .split('\n')
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .map(|s| s.chars().collect::<Vec<_>>())
            .collect();

        assert_eq!(count_occupied(&input, 4, 3), 8);
        assert_eq!(count_occupied(&input, 0, 8), 2);
    }
    #[test]
    fn test() {
        let input = r"L.LL.LL.LL
        LLLLLLL.LL
        L.L.L..L..
        LLLL.LL.LL
        L.LL.LL.LL
        L.LLLLL.LL
        ..L.L.....
        LLLLLLLLLL
        L.LLLLLL.L
        L.LLLLL.LL";

        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 26);
    }
}
