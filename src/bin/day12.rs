#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input12")?;
    let result = solve(&input)?;
    println!("{}", result);
    Ok(())
}

fn solve(input: &str) -> Result<i32, Box<dyn Error + Send + Sync>> {
    let input: Vec<_> = input
        .split('\n')
        .map(|s| s.trim())
        .filter(|s| !s.is_empty())
        .map(|s| {
            let cs: Vec<_> = s.chars().collect();
            let dir = cs[0];
            let num: String = cs[1..].iter().collect();
            let num = num.parse::<i32>().unwrap();
            (dir, num)
        })
        .collect();

    let mut ship_pos = (0, 0);
    let mut waypoint_pos = (10, 1); // relative to the ship!

    for (dir, v) in input {
        match dir {
            'N' => waypoint_pos.1 += v,
            'S' => waypoint_pos.1 -= v,
            'E' => waypoint_pos.0 += v,
            'W' => waypoint_pos.0 -= v,
            'L' => {
                waypoint_pos = rotate_vec(waypoint_pos, v);
            }
            'R' => {
                waypoint_pos = rotate_vec(waypoint_pos, -v);
            }
            'F' => {
                ship_pos.0 += waypoint_pos.0 * v;
                ship_pos.1 += waypoint_pos.1 * v;
            }
            _ => {}
        }
    }

    let result = ship_pos.0.abs() + ship_pos.1.abs();

    Ok(result)
}
fn rotate_vec(v: (i32, i32), ang: i32) -> (i32, i32) {
    let ang = (ang as f32).to_radians();
    let v = (v.0 as f32, v.1 as f32);
    let x = ang.cos() * v.0 - ang.sin() * v.1;
    let y = ang.sin() * v.0 + ang.cos() * v.1;
    (x.round() as i32, y.round() as i32)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn ang_to_vec_test() {
        assert_eq!(rotate_vec((1, 0), 180), (-1, 0));
        assert_eq!(rotate_vec((0, 1), 180), (0, -1));
        assert_eq!(rotate_vec((10, 1), 180), (-10, -1));
        assert_eq!(rotate_vec((10, 1), -180), (-10, -1));
    }
    #[test]
    fn test() {
        let input = r"F10
        N3
        F7
        R90
        F11";
        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 286);
    }

    #[test]
    fn test_1() {
        let input = r"F20
        R180
        F20";
        let result = solve(input);
        assert!(result.is_ok());

        let result = result.unwrap();
        assert_eq!(result, 0);
    }
}
