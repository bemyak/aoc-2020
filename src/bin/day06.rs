#[allow(unused_imports)]
use aoc_2020::EasilyParsable;
use std::collections::HashSet;
use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input06")?;
    let result = solve02(&input)?;
    println!("{}", result);
    Ok(())
}

#[allow(dead_code)]
fn solve01(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let result: Vec<_> = input
        .split("\n\n")
        .filter(|s| !s.is_empty())
        .map(|s| {
            s.chars()
                .filter(|c| c.is_alphabetic())
                .collect::<HashSet<_>>()
                .len()
        })
        .collect();
    println!("{:?}", result);
    Ok(result.iter().sum())
}
fn solve02(input: &str) -> Result<usize, Box<dyn Error + Send + Sync>> {
    let result: Vec<_> = input
        .split("\n\n")
        .filter(|s| !s.is_empty())
        .filter_map(|s| {
            s.split('\n')
                .map(|s| {
                    s.chars()
                        .filter(|c| c.is_alphabetic())
                        .collect::<HashSet<_>>()
                })
                .fold(None, |acc, new_set| {
                    let acc: Option<HashSet<_>> = acc;
                    match acc {
                        Some(prev_set) => Some(
                            prev_set
                                .intersection(&new_set)
                                .copied()
                                .collect::<HashSet<_>>(),
                        ),
                        None => Some(new_set),
                    }
                })
        })
        .map(|set| set.len())
        .collect();
    println!("{:?}", result);
    Ok(result.iter().sum())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let input = r"abc

a
b
c

ab
ac

a
a
a
a

b";

        let result01 = solve01(input);
        assert!(result01.is_ok());
        let result = result01.unwrap();
        assert_eq!(result, 11);

        let result02 = solve02(input);
        assert!(result02.is_ok());
        let result = result02.unwrap();
        assert_eq!(result, 6);
    }
}
