use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input05")?;
    let result = solve(&input)?;
    println!("{:?}", result);
    Ok(())
}

fn solve(input: &str) -> Result<i32, Box<dyn Error + Send + Sync>> {
    let seats = get_seats(input);
    let result = find_missing(&seats);
    Ok(result)
}

fn get_seats(input: &str) -> Vec<i32> {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| s.chars().collect::<Vec<_>>())
        .map(|s| {
            let r = bin_search((0, 127), &s[..7]);
            let c = bin_search((0, 7), &s[7..]);
            r * 8 + c
        })
        .collect()
}

fn bin_search((lo, hi): (i32, i32), code: &[char]) -> i32 {
    if code.is_empty() {
        return lo;
    }

    match code[0] {
        'F' | 'L' => bin_search((lo, (lo + hi) / 2), &code[1..]),
        'B' | 'R' => bin_search(((lo + hi) / 2 + 1, hi), &code[1..]),
        _ => panic!("Unknown direction"),
    }
}

fn find_missing(seats: &[i32]) -> i32 {
    for r in 0..128 {
        for c in 0..7 {
            let seat = r * 8 + c;
            if seat < seats[0] {
                continue;
            }
            if !seats.contains(&seat) {
                return seat;
            }
        }
    }
    0
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_get_seats_max() {
        assert!(get_seats("FBFBBFFRLR").iter().max().is_some());
        assert_eq!(get_seats("FBFBBFFRLR").iter().max().unwrap(), &357);
        assert_eq!(get_seats("BFFFBBFRRR").iter().max().unwrap(), &567);
        assert_eq!(get_seats("FFFBBBFRRR").iter().max().unwrap(), &119);
        assert_eq!(get_seats("BBFFBBFRLL").iter().max().unwrap(), &820);
    }
    #[test]
    fn test_bin_search() {
        assert_eq!(
            bin_search((0, 127), &"FBFBBFF".chars().collect::<Vec<_>>()),
            44
        );
    }
}
