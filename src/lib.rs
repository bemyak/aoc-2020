use std::{error::Error, fmt::Display};

#[derive(Debug)]
pub struct DummyError;

impl Display for DummyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Duh")
    }
}

impl Error for DummyError {}

pub trait EasilyParsable: Sized {
    fn split_once_(&self, sep: char) -> Option<(Self, Self)>;
    fn split_once_s(&self, sep: &str) -> Option<(Self, Self)>;
}

impl EasilyParsable for &str {
    fn split_once_(&self, sep: char) -> Option<(Self, Self)> {
        let mut split = self.splitn(2, sep);
        Some((
            split.next().map(|s| s.trim())?,
            split.next().map(|s| s.trim())?,
        ))
    }

    fn split_once_s(&self, sep: &str) -> Option<(Self, Self)> {
        let mut split = self.splitn(2, sep);
        Some((
            split.next().map(|s| s.trim())?,
            split.next().map(|s| s.trim())?,
        ))
    }
}
impl EasilyParsable for String {
    fn split_once_(&self, sep: char) -> Option<(Self, Self)> {
        let mut split = self.splitn(2, sep);
        Some((
            split.next().map(|s| s.trim().to_owned())?,
            split.next().map(|s| s.trim().to_owned())?,
        ))
    }
    fn split_once_s(&self, sep: &str) -> Option<(Self, Self)> {
        let mut split = self.splitn(2, sep);
        Some((
            split.next().map(|s| s.trim().to_owned())?,
            split.next().map(|s| s.trim().to_owned())?,
        ))
    }
}
